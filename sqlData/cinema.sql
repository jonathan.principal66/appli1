-- MySQL dump 10.13  Distrib 8.0.31, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: cinema
-- ------------------------------------------------------
-- Server version	8.0.29

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `acteur`
--

LOCK TABLES `acteur` WRITE;
/*!40000 ALTER TABLE `acteur` DISABLE KEYS */;
INSERT INTO `acteur` VALUES (21,'Guillaume','Jacqueline'),(22,'Albert','David'),(23,'Petitjean','Thomas'),(24,'Carpentier','Margaux'),(25,'Chartier','Christelle'),(26,'Richard','Andrée'),(27,'Riou','Patricia'),(28,'Grondin','Augustin'),(29,'Hernandez','Martin'),(30,'Paris','Andrée'),(31,'Thierry','Madeleine'),(32,'Cordier','Timothée'),(33,'Simon','Vincent'),(34,'Maillot','Marc'),(35,'Aubert','Thérèse'),(36,'Nguyen','Marc'),(37,'Alves','Jérôme'),(38,'Vaillant','Michelle'),(39,'Lefort','Aimée'),(40,'Dubois','Martin'),(41,'Gilles','Laurence'),(42,'Blanchard','Yves'),(43,'Allard','Éric'),(44,'Rodrigues','Anouk'),(45,'Thibault','Théophile'),(46,'Lebon','Édouard'),(47,'Blot','Julie'),(48,'Denis','André'),(49,'Monnier','Gérard'),(50,'Dumont','Aimée'),(51,'Da Silva','Aimé'),(52,'Roussel','Valérie'),(53,'Mercier','Emmanuelle'),(54,'Cordier','Michel'),(55,'Gauthier','Julien'),(56,'Rolland','Élisabeth'),(57,'Vincent','Thibaut'),(58,'Fournier','Richard'),(59,'Simon','Frédéric'),(60,'Morel','Margot'),(61,'Blot','Stéphanie'),(62,'Bonnin','Philippe'),(63,'Andre','Margaux'),(64,'Renault','Daniel'),(65,'Ferreira','Monique'),(66,'Millet','Vincent'),(67,'Marchal','Daniel'),(68,'Merle','Nicole'),(69,'Payet','Manon'),(70,'Colin','Louis'),(71,'Morin','Constance'),(72,'Raynaud','Valérie'),(73,'Joseph','Nicolas'),(74,'Jacques','Édouard'),(75,'Alexandre','Arnaude'),(76,'Leleu','Denis'),(77,'Jourdan','Véronique'),(78,'Gros','Sabine'),(79,'Poirier','Roland'),(80,'Hernandez','Vincent'),(81,'Hardy','Honoré'),(82,'Vallet','Margot'),(83,'Da Costa','Marthe'),(84,'Dupre','Julie'),(85,'Leleu','Thierry'),(86,'Becker','Frédéric'),(87,'Fernandes','Roger'),(88,'Godard','Dominique'),(89,'Monnier','Marcelle'),(90,'Samson','Aurore'),(91,'Jacob','Catherine'),(92,'Giraud','Augustin'),(93,'Devaux','Caroline'),(94,'Thomas','Alphonse'),(95,'Martins','Vincent'),(96,'Dupuy','Laure'),(97,'Fouquet','Marie'),(98,'Faure','Chantal'),(99,'Boulay','Xavier'),(100,'Lebreton','Margaret'),(101,'Pichon','Sophie'),(102,'Fischer','Gérard'),(103,'Bourgeois','Philippe'),(104,'Lebon','Geneviève'),(105,'Poirier','Olivie'),(106,'Gaudin','Éléonore'),(107,'Coste','Théophile'),(108,'Peron','Zacharie'),(109,'Etienne','Anastasie'),(110,'Cousin','Olivier'),(111,'Thomas','Dorothée'),(112,'Gautier','Marguerite'),(113,'Foucher','Célina'),(114,'Leleu','Sophie'),(115,'Moulin','Daniel'),(116,'Lefevre','Thierry'),(117,'Deschamps','Émile'),(118,'Guyon','Jacques'),(119,'Maurice','Yves'),(120,'Chretien','Georges'),(121,'Petitjean','Thomas'),(122,'Guillaume','Jacqueline'),(123,'Albert','David'),(124,'Chretien','Georges'),(125,'Petitjean','Thomas'),(126,'Guillaume','Jacqueline'),(127,'Albert','David');
/*!40000 ALTER TABLE `acteur` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `acteur_film`
--

LOCK TABLES `acteur_film` WRITE;
/*!40000 ALTER TABLE `acteur_film` DISABLE KEYS */;
INSERT INTO `acteur_film` VALUES (25,1),(36,1),(45,1),(45,2),(46,2),(47,2),(121,3),(124,7),(125,7),(126,7),(127,7);
/*!40000 ALTER TABLE `acteur_film` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `doctrine_migration_versions`
--

LOCK TABLES `doctrine_migration_versions` WRITE;
/*!40000 ALTER TABLE `doctrine_migration_versions` DISABLE KEYS */;
INSERT INTO `doctrine_migration_versions` VALUES ('DoctrineMigrations\\Version20230201104443','2023-02-01 10:47:11',321),('DoctrineMigrations\\Version20230201111714','2023-02-01 11:17:54',2457);
/*!40000 ALTER TABLE `doctrine_migration_versions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `film`
--

LOCK TABLES `film` WRITE;
/*!40000 ALTER TABLE `film` DISABLE KEYS */;
INSERT INTO `film` VALUES (1,'Le grand cirque','A l\'occasion d\'une visite à un ami à l\'hôpital pédiatrique Robert Debré à Paris, Momo, un comédien en manque de rôles, fait la rencontre de Michel, le directeur d\'une association de clowns bénévoles, qui rend visite aux enfants hospitalisés. Grace aux encouragements et à la confiance de Michel, Momo se laisse convaincre de jouer un rôle dans la vie de ces enfants. Son défi : les faire rire malgré la maladie. Très rapidement, les enfants tombent sous son charme cartoonesque et Momo fait partie intégrante du service pédiatrique. Avec la complicité de Michel son mentor et de Bénédicte, une infirmière de l\'hôpital, Momo va avoir l\'idée de mettre en scène un spectacle de cirque au sein de l\'hôpital dans lequel chaque enfant aura un rôle déterminant…','2023-02-15','02:00:00','https://fr.web.img3.acsta.net/c_155_210_50_50/pictures/22/12/14/17/06/1834896.jpg',13),(2,'Alibi.com 2','Après avoir fermé son agence Alibi.com et promis à Flo qu\'il ne lui mentirait plus jamais, la nouvelle vie de Greg est devenue tranquille, trop tranquille... Plus pour longtemps! Lorsqu\'il décide de demander Flo en mariage, Greg est au pied du mur et doit se résoudre à présenter sa famille. Mais entre son père escroc et sa mère ex-actrice de films de charme, ça risque fort de ruiner sa future union. Il n\'a donc pas d\'autre choix que de réouvrir son agence avec ses anciens complices pour un ultime Alibi et de se trouver des faux parents plus présentables...','2023-02-08','01:30:00','https://fr.web.img3.acsta.net/c_155_210_50_50/pictures/22/12/21/17/30/5654138.jpg',15),(3,'sacrées momies','Vous êtes-vous déjà demandé à quoi ressemblait le monde des momies ? Sous les pyramides d\'Egypte se trouve un royaume fabuleux, où les momies conduisent des chars, rêvent de devenir des pop stars et vivent à l\'écart de la civilisation humaine. Mais lorsqu\'un archéologue sans scrupule pille un de leur trésors, Thut et la princesse Nefer, fiancés malgré eux, se voient contraints de faire équipe et de se rendre dans le monde des vivants. Accompagnés par le frère de Thut et son crocodile domestique, ils vont vivre une aventure hors du commun à Londres et former une amitié inattendue.','2023-02-08','02:10:00','https://fr.web.img3.acsta.net/c_155_210_50_50/pictures/23/01/05/15/42/0230478.jpg',12),(7,'Astérix et Obélix : L\'Empire du milieu','Nous sommes en 50 avant J.C. L\'Impératrice de Chine est emprisonnée suite à un coup d\'état fomenté par Deng Tsin Quin, un prince félon. Aidée par Graindemaïs, le marchand phénicien, et par sa fidèle guerrière Tat Han, la princesse Fu Yi, fille unique de l\'impératrice, s\'enfuit en Gaule pour demander de l\'aide aux deux valeureux guerriers Astérix et Obélix, dotés d\'une force surhumaine grâce à leur potion magique. Nos deux inséparables Gaulois acceptent bien sûr de venir en aide à la Princesse pour sauver sa mère et libérer son pays. Et les voici tous en route pour une grande aventure vers la Chine. Mais César et sa puissante armée, toujours en soif de conquêtes, ont eux aussi pris la direction de l\'Empire du Milieu…','2023-02-01','01:51:00','https://fr.web.img4.acsta.net/c_155_210_50_50/pictures/23/01/03/15/53/0336388.jpg',15),(8,'BTS : Yet To Come in Cinemas','Retrouvez RM, Jin, SUGA, j-hope, Jimin, V and Jung Kook dans cette version cinéma inédite spécialement remontée et remixée pour le grand écran. Découvrez de nouveaux plans rapprochés et angles de vue sur l\'ensemble du concert BTS: Yet To Come au cinéma. Avec les plus gros tubes du groupe parmi lesquels \"Dynamite\", \"Butter\" et \"IDOL\", ainsi que la première performance live de \"Run BTS\", single du dernier album du groupe, Proof. Rendez-vous dans les cinémas du monde entier pour ce moment de célébration. Que la fête commence !','2023-02-01','01:43:00','https://fr.web.img3.acsta.net/c_155_210_50_50/pictures/23/01/05/15/40/2754659.jpg',12);
/*!40000 ALTER TABLE `film` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `genre`
--

LOCK TABLES `genre` WRITE;
/*!40000 ALTER TABLE `genre` DISABLE KEYS */;
INSERT INTO `genre` VALUES (1,'Comédie'),(2,'Drame'),(3,'Romance'),(4,'Action'),(5,'Thriller'),(6,'Horreur'),(7,'Science-fiction'),(8,'Fantastique'),(9,'Policier'),(10,'Western'),(11,'Musique'),(12,'Aventure'),(13,'Animation'),(14,'Documentaire'),(15,'Biopic'),(16,'Guerre'),(17,'Comédie musicale'),(18,'Erotique'),(19,'Sport'),(20,'Film noir'),(21,'Film d\'art'),(22,'Animation'),(23,'Comédie'),(24,'Drame'),(25,'Comédie'),(26,'Aventure'),(27,'Musique');
/*!40000 ALTER TABLE `genre` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `genre_film`
--

LOCK TABLES `genre_film` WRITE;
/*!40000 ALTER TABLE `genre_film` DISABLE KEYS */;
INSERT INTO `genre_film` VALUES (1,1),(1,2),(22,3),(25,7),(26,7),(27,8);
/*!40000 ALTER TABLE `genre_film` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `messenger_messages`
--

LOCK TABLES `messenger_messages` WRITE;
/*!40000 ALTER TABLE `messenger_messages` DISABLE KEYS */;
/*!40000 ALTER TABLE `messenger_messages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `realisateur`
--

LOCK TABLES `realisateur` WRITE;
/*!40000 ALTER TABLE `realisateur` DISABLE KEYS */;
INSERT INTO `realisateur` VALUES (11,'Michaud','Stéphanie'),(12,'Pruvost','Martin'),(13,'Lemoine','Élise'),(14,'Brunet','Jacqueline'),(15,'Robin','Émile');
/*!40000 ALTER TABLE `realisateur` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-02-02 12:31:01
