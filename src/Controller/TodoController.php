<?php

namespace App\Controller;

use App\data\Todo;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/todo')]
class TodoController extends AbstractController
{

    #[Route('/', name: 'app_todo', methods: "GET")]
    public function index(Request $request): Response
    {
        // on récupère la session en cours
        $session = $request->getSession();

        // on récupère todolist dans la session
        $todolist = $session->get('todolist');

        // Si todolist n'existe pas je crée la variable dans la session
        if (!$todolist) {
            $session->set('todolist', $this->init());
        }

        return $this->render('todo/index.html.twig', [
            'controller_name' => 'TodoController',
            'todolist' => $session->get('todolist')
        ]);
    }

    #[Route('/detail/{id}', name: 'app_todo.detail', methods: "GET")]
    public function detail(Request $request, int $id): Response
    {
        // on récupère la session en cours
        $session = $request->getSession();

        // on récupère todolist dans la session
        $todolist = $session->get('todolist');
        $result = null;
        foreach ($todolist as $todo) {
            if ($todo->id === $id) {
                $result = $todo;
            }
        }

        if ($result == null) {
            $this->addFlash('warning', 'la todo que vous tentez d\'afficher n\'existe pas');
        }

        return $this->render('todo/detail.html.twig', [
            'controller_name' => 'TodoController',
            'todo' => $result
        ]);
    }
    #[Route('/delete/{id}', name: 'app_todo.delete', methods: "GET")]
    public function delete(Request $request, int $id)
    {
       $session = $request->getSession();
        $todolist = $session->get('todolist');
        // je parcours la todolist, et je supprime la todo par son index ($key)
        foreach ($todolist as $key => $todo) {
            if ($todo->id == $id) {
                unset($todolist[$key]);
            }
        }
        // j'ecrase le tableau precedent par le nouveau dans la session
        $session->set('todolist', $todolist);

        // je fais une redirection vers la page par defaut de todo
        return $this->redirect("/todo");
    }

    #[Route('/patch/completed/{id}', name: 'app_todo.patch.completed', methods: "GET")]
    public function patchCompleted(Request $request, int $id): Response
    {
        $session = $request->getSession();
        $todolist = $session->get('todolist');
        // je parcours la todolist, et je supprime la todo par son index ($key)
        foreach ($todolist as $key => $todo) {
            if ($todo->id == $id) {
                $todo->completed = !$todo->completed;
            }
    }
        $session->set('todolist', $todolist);
        return $this->redirect('/todo');
}

    private function init(): array
    {
        return [
            new Todo("Apprendre symfony", "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus, obcaecati!"),
            new Todo("Créer un constroller", "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus, obcaecati!"),
            new Todo("Manipuler les données", "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus, obcaecati!"),
        ];
    }
}
