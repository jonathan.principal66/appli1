<?php

namespace App\data;

class Todo
{

    public int $id;

    public string $task;

    public string $description;

    public bool $completed;

    private static int $count = 1;

    public function __construct($task, $description)
    {
        $this->completed = false;
        $this->task = $task;
        $this->id = self::$count;
        $this->description = $description;
        self::$count++;
    }

}